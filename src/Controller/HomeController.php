<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{

    /**
     * @Route("/hello/{prenoms}/age/{age}", name="hello")
     * @Route("/hello", name="hellom")
     * @Route("/salut", name="slt")
     * 
     * show page how say hello
     */
    public function hello($prenom = "nono", $age = 0)
    {
        return $this->render(
            'hello.html.twig',
            [
                'prenom' => $prenom,
                'age' => $age
            ]
        );
    }

    /**
     * @Route("/", name="homepage")
     */
    public function home() 
    {
        $prenoms = ["Naws" => 10, "Lea" => 20, "Pol" => 30];

        return $this->render(
            'home.html.twig',
            [
                'title' => "Bonjour à tous !§§",
                'age' => 15,
                'tableau' => $prenoms
                
            ]
        );
    }
}